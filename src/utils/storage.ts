const prefix: string = process.env.VUE_APP_PREFIX;

const set = (key: string, value: any): void => {
  localStorage.setItem(`${prefix}${key}`, JSON.stringify(value));
};

const get = (key: string): any => JSON.parse(localStorage.getItem(`${prefix}${key}`) || 'null');

export default {
  get,
  set,
};
