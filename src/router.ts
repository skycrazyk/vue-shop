import Vue from 'vue';
import Router, { RouterOptions } from 'vue-router';
import Home from '@/views/Home.vue';

Vue.use(Router);

const config: RouterOptions = {
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import(/* webpackChunkName: "cart" */ './views/Cart.vue'),
    },
  ],
};

export default new Router(config);
