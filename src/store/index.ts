import Vue from 'vue';
import Vuex, { Plugin } from 'vuex';
import storage from '@/utils/storage';
import Catalog from '@/store/catalog/';
import Cart from '@/store/cart/';
import { getModule } from 'vuex-module-decorators';

export interface IRootState {
  catalog: Catalog;
  cart: Cart;
}

Vue.use(Vuex);

const cartPlugin: Plugin<IRootState> = (store) => {
  store.subscribe((mutation, state) => {
    if (/^cart\/.+$/.test(mutation.type)) {
      storage.set('cart', state.cart.cart);
    }
  });
};

const store = new Vuex.Store<IRootState>({
  modules: {
    cart: Cart,
    catalog: Catalog,
  },
  strict: true,
  plugins: [cartPlugin],
});

export default store;

export const CartModule = getModule(Cart, store);
export const CatalogModule = getModule(Catalog, store);
