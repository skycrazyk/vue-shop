import {
  Module,
  VuexModule,
  Mutation,
} from 'vuex-module-decorators';
import storage from '@/utils/storage';
import { ICatalogProduct } from '@/store/catalog/';

export interface ICartProduct {
  id: number;
  name: string;
  img: string;
  price: number;
  count: number;
}

export interface ICartState {
  cart: ICartProduct[];
}

@Module({
  name: 'cart',
  namespaced: true,
})
export default class Cart extends VuexModule implements ICartState {
  cart: ICartProduct[] = storage.get('cart') || [];

  get count() {
    return this.cart.reduce((acc: number, current) => acc + current.count, 0);
  }

  get total() {
    return this.cart.reduce((acc: number, current) => acc + (current.price * current.count), 0);
  }

  @Mutation
  increment(product: ICartProduct | ICatalogProduct) {
    const same = this.cart.find(item => item.id === product.id);
    if (same) {
      same.count += 1;
    } else {
      this.cart.unshift({
        ...product,
        count: 1,
      });
    }
  }

  @Mutation
  decrement(product: ICartProduct) {
    const same = this.cart.find(item => item.id === product.id);
    if (same) {
      same.count -= 1;
      if (!same.count) {
        const index = this.cart.indexOf(same);
        this.cart.splice(index, 1);
      }
    }
  }

  @Mutation
  editCount(product: ICartProduct) {
    product.count = Math.round(Math.abs(product.count)); // eslint-disable-line no-param-reassign

    if (product.count) {
      const same = this.cart.find(item => item.id === product.id);
      if (same) {
        same.count = product.count;
      }
    } else {
      const index = this.cart.findIndex(item => item.id === product.id);
      this.cart.splice(index, 1);
    }
  }

  @Mutation
  deleteAll() {
    this.cart = []; // eslint-disable-line no-param-reassign
  }
}
