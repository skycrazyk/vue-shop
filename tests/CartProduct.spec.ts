import {
  mount,
  createLocalVue,
} from '@vue/test-utils';
import CartProduct from '@/components/CartProduct.vue';
import { ICartProduct } from '@/store/cart/';

describe('CartProduct', () => {
  const product: ICartProduct = {
    id: 1,
    name: 'Node.js',
    img: 'catalog/nodejs.png',
    price: 5.5,
    count: 2,
  };

  test('Has preview', () => {
    const wrapper = mount(CartProduct, {
      propsData: {
        product,
      },
    });
    expect(wrapper.find('img.img').attributes('src')).toBe('catalog/nodejs.png');
  });

  test('Product name is Node.js', () => {
    const wrapper = mount(CartProduct, {
      propsData: {
        product,
      },
    });
    expect(wrapper.find('td:nth-child(2)').text()).toBe('Node.js');
  });

  test('Price is $5.50', () => {
    const wrapper = mount(CartProduct, {
      propsData: {
        product,
      },
    });
    expect(wrapper.find('td:nth-child(3)').text()).toBe('$5.50');
  });

  test('Count is 2', () => {
    const wrapper = mount(CartProduct, {
      propsData: {
        product,
      },
    });
    const input = <HTMLInputElement>wrapper.find('.field').element;
    expect(input.value).toBe('2');
  });
});
