module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/extensions': ['error', 'always', {
      js: 'never',
      // vue: 'never',
      ts: 'never'
    }],
  },

  parserOptions: {
    parser: 'typescript-eslint-parser',
  },

  overrides: [
    {
      files: ['*.spec.js'],
      globals: {
        describe: false,
        beforeEach: false,
        test: false,
        expect: false,
        jest: false,
      }
    },
  ],

  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript'
  ]
};
